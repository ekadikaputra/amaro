<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use App\about;
use App\galeri;
use App\portofolio;
use App\kontak;
use App\partner;
use App\testi;
use App\feedback;

class FrontController extends Controller
{
   
    public function index()
    {
       $about   = about::first();

       // orderByRaw('RAND()')->
       $gallery = galeri::take(3)->get();
       $porto = portofolio::take(3)->get();

       $contact = kontak::first();

       $testi   = testi::take(3)->get();
      
       return view('layout', compact('about', 'gallery', 'contact', 'testi', 'porto'));
   }

   public function post(Request $request)
    {
        $validator = Validator::make($request->all(),[
           'fullname' => 'required|max:70',
           'email' => 'required|email',
           'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ;
        }
        
        $input = $request->all();
        if ($input) {
            $input['created_at'] = \Carbon\Carbon::now();
            feedback::create($input);
        }
      
       return redirect()->back()->with('pesan', 'Pesan Terkirim!');
       // return view('layout')->with(compact('about', 'gallery', 'contact'));
   }
   
}
