<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\about;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class aboutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
               {
                   if ($request->ajax()) {
                   $abouts = about::select(['id', 'visi', 'misi', 'nilai_perusahaan', 'profil']);
                   return Datatables::of($abouts)
                    ->addColumn('action', function($about){
                        return view('datatable._action3', [
                            'model' => $about,
                            'form_url' => route('abouts.destroy', $about->id),
                            'edit_url' => route('abouts.edit', $about->id),
                            'confirm_message' => 'Yakin mau menghapus ?'
                            ]);
                        })->make(true);
                    }
               
                   $html = $htmlBuilder
                       ->addColumn(['data' => 'visi', 'name'=>'visi', 'title'=>'Visi'])
                       ->addColumn(['data' => 'misi', 'name'=>'misi', 'title'=>'Misi'])
                       ->addColumn(['data' => 'nilai_perusahaan', 'name'=>'nilai_perusahaan', 'title'=>'Nilai Perusahaan'])
                       ->addColumn(['data' => 'profil', 'name'=>'profil', 'title'=>'Profil'])
                       ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
                   
                   return view('abouts.index')->with(compact('html'));
               }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    $about = about::find($id);
    return view('abouts.edit')->with(compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['visi' => 'required|unique:abouts,visi,'. $id]);
        $this->validate($request, ['misi' => 'required|unique:abouts,misi,'. $id]);
        $this->validate($request, ['nilai_perusahaan' => 'required|unique:abouts,nilai_perusahaan,'. $id]);
        $this->validate($request, ['profil' => 'required|unique:abouts,profil,'. $id]);        

        $about = about::find($id);
        $about->update($request->all());
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan"
        ]);
        return redirect()->route('abouts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        about::destroy($id);
    
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil dihapus"
        ]);
        
        return redirect()->route('abouts.index');
    }
}
