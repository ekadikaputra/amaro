<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\feedback;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class feedbacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
       {
           if ($request->ajax()) {
               $feedbacks = feedback::select(['id', 'fullname', 'email', 'message']);
               return Datatables::of($feedbacks)
                   ->addColumn('action', function($feedback){
                       return view('datatable._action2', [
                           'model' => $feedback,
                           'form_url' => route('feedbacks.destroy', $feedback->id),
                           'edit_url' => route('feedbacks.edit', $feedback->id),
                           'confirm_message' => 'Yakin mau menghapus ?'
                       ]);
                   })->make(true);
           }
           
           $html = $htmlBuilder
               ->addColumn(['data' => 'fullname', 'name'=>'fullname', 'title'=>'Nama Lengkap'])
               ->addColumn(['data' => 'email', 'name'=>'email', 'title'=>'Email'])
               ->addColumn(['data' => 'message', 'name'=>'message', 'title'=>'Pesan'])
               ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
           
           return view('feedbacks.index')->with(compact('html'));
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feedback = feedback::find($id);
        return view('feedbacks.edit')->with(compact('feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
        {
           //  $this->validate($request, ['no_hp' => 'required|unique:kontaks,no_hp,'. $id]);
           //  $this->validate($request, ['jam_kerja' => 'required|unique:kontaks,jam_kerja,'. $id]);
           //  $this->validate($request, ['alamat' => 'required|unique:kontaks,alamat,'. $id]);
            
           //  $kontak = feedback::find($id);
           //  $kontak->update($request->all());
            
           //  Session::flash("flash_notification", [
           //     "level"=>"success",
           //     "message"=>"Berhasil mengedit"
           // ]);
           // return redirect()->route('feedbacks.index');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        feedback::destroy($id);
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Feedback berhasil dihapus"
        ]);
        
        return redirect()->route('feedbacks.index');
    }
}
