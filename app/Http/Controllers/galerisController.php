<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\galeri;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class galerisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $galeris = galeri::select(['id', 'keterangan', 'gambar']);
            return Datatables::of($galeris)
                ->addColumn('action', function($galeri){
                    return view('datatable._action', [
                        'model' => $galeri,
                        'form_url' => route('galeris.destroy', $galeri->id),
                        'edit_url' => route('galeris.edit', $galeri->id),
                        'confirm_message' => 'Yakin mau menghapus ' . $galeri->keterangan . '?'
                    ]);
                })->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data' => 'keterangan', 'name'=>'keterangan', 'title'=>'Keterangan'])
        ->addColumn(['data' => 'gambar', 'name'=>'gambar', 'title'=>'Gambar'])
        ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);

        return view('galeris.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('galeris.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
       'keterangan' => 'required|unique:galeris,keterangan',
       'gambar' => 'image|max:2048'
       ]);

       $galeri = galeri::create($request->except('gambar'));
       
       // isi field cover jika ada cover yang diupload
       if ($request->hasFile('gambar')) {
       
       // Mengambil file yang diupload
       $uploaded_gambar = $request->file('gambar');
       
       // mengambil extension file
       $extension = $uploaded_gambar->getClientOriginalExtension();
       
       // membuat nama file random berikut extension
       $filename = md5(time()) . '.' . $extension;
       
       // menyimpan cover ke folder public/img
       $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'galeri';
       $uploaded_gambar->move($destinationPath, $filename);
       
       // mengisi field cover di book dengan filename yang baru dibuat
       $galeri->gambar = $filename;
       $galeri->save();
       }
       
       Session::flash("flash_notification", [
        "level"=>"success",
        "message"=>"Berhasil menyimpan gambar $galeri->keterangan"
       ]);
       
       return redirect()->route('galeris.index');
       }
    


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeri = galeri::find($id);
        return view('galeris.edit')->with(compact('galeri'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
                   'keterangan' => 'required|unique:galeris,keterangan,' . $id,
                   'gambar' => 'image|max:2048'
               ]);
               
               $galeri = galeri::find($id);
               $galeri->update($request->all());
               
               if ($request->hasFile('gambar')) {
               // menambil cover yang diupload berikut ekstensinya
                   $filename = null;
                   $uploaded_gambar = $request->file('gambar');
                   $extension = $uploaded_gambar->getClientOriginalExtension();
               
               // membuat nama file random dengan extension
                   $filename = md5(time()) . '.' . $extension;
                   $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'galeri';
               
               // memindahkan file ke folder public/img
                   $uploaded_gambar->move($destinationPath, $filename);
               
               // hapus cover lama, jika ada
                   if ($galeri->gambar) {
                   $old_gambar = $galeri->gambar;
                   $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                       . DIRECTORY_SEPARATOR . $galeri->gambar;
               
                   try {
                       File::delete($filepath);
                   } catch (FileNotFoundException $e) {
               // File sudah dihapus/tidak ada
                   }
               }
               // ganti field cover dengan cover yang baru
                   $galeri->gambar = $filename;
                   $galeri->save();
               }
               
               Session::flash("flash_notification", [
                   "level"=>"success",
                   "message"=>"Berhasil menyimpan $galeri->keterangan"
               ]);
               
               return redirect()->route('galeris.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galeri = galeri::find($id);
        
        // hapus cover lama, jika ada
        if ($galeri->gambar) {
        $old_gambar = $galeri->gambar;
        $filepath = public_path() . DIRECTORY_SEPARATOR . 'galeri'
        . DIRECTORY_SEPARATOR . $galeri->gambar;
        
        try {
        File::delete($filepath);
        } catch (FileNotFoundException $e) {
        // File sudah dihapus/tidak ada
        }
        }
        
        $galeri->delete();
        
        Session::flash("flash_notification", [
        "level"=>"success",
        "message"=>"Gambar berhasil dihapus"
        ]);
        return redirect()->route('galeris.index');
    }
}
