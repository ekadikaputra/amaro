<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kontak;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class kontaksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            $kontaks = kontak::select(['id', 'no_hp', 'jam_kerja', 'alamat']);
            return Datatables::of($kontaks)
                ->addColumn('action', function($kontak){
                    return view('datatable._action3', [
                        'model' => $kontak,
                        'form_url' => route('kontaks.destroy', $kontak->id),
                        'edit_url' => route('kontaks.edit', $kontak->id),
                        'confirm_message' => 'Yakin mau menghapus ?'
                    ]);
                })->make(true);
        }
        
        $html = $htmlBuilder
            ->addColumn(['data' => 'no_hp', 'name'=>'no_hp', 'title'=>'No. HP'])
            ->addColumn(['data' => 'jam_kerja', 'name'=>'jam_kerja', 'title'=>'Jam Kerja'])
            ->addColumn(['data' => 'alamat', 'name'=>'alamat', 'title'=>'Alamat'])
            ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
        
        return view('kontaks.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kontak = kontak::find($id);
        return view('kontaks.edit')->with(compact('kontak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
        {
            $this->validate($request, ['no_hp' => 'required|unique:kontaks,no_hp,'. $id]);
            $this->validate($request, ['jam_kerja' => 'required|unique:kontaks,jam_kerja,'. $id]);
            $this->validate($request, ['alamat' => 'required|unique:kontaks,alamat,'. $id]);
            
            $kontak = kontak::find($id);
            $kontak->update($request->all());
            
            Session::flash("flash_notification", [
               "level"=>"success",
               "message"=>"Berhasil mengedit"
           ]);
           return redirect()->route('kontaks.index');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        kontak::destroy($id);
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Kontak berhasil dihapus"
        ]);
        
        return redirect()->route('kontaks.index');
    }
}
