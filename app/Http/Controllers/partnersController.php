<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\partner;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class partnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request, Builder $htmlBuilder)
           {
               if ($request->ajax()) {
               $partners = partner::select(['id', 'nama_perusahaan', 'logo']);
               return Datatables::of($partners)
                ->addColumn('action', function($partner){
                    return view('datatable._action', [
                        'model' => $partner,
                        'form_url' => route('partners.destroy', $partner->id),
                        'edit_url' => route('partners.edit', $partner->id),
                        'confirm_message' => 'Yakin mau menghapus logo ' . $partner->nama_perusahaan . '?'
                        ]);
                    })->make(true);
               }
           
               $html = $htmlBuilder
                   ->addColumn(['data' => 'nama_perusahaan', 'name'=>'nama_perusahaan', 'title'=>'Nama Perusahaan'])
                   ->addColumn(['data' => 'logo', 'name'=>'logo', 'title'=>'Logo'])
                   ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
               
               return view('partners.index')->with(compact('html'));
           }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create()
       {
          return view('partners.create');
       }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_perusahaan' => 'required|unique:partners,nama_perusahaan',
            'logo' => 'image|max:2048'
        ]);
    
        $partner = partner::create($request->except('logo'));
        
        // isi field cover jika ada cover yang diupload
        if ($request->hasFile('logo')) {
        // Mengambil file yang diupload
        $uploaded_logo = $request->file('logo');
            // mengambil extension file
            $extension = $uploaded_logo->getClientOriginalExtension();
        
            // membuat nama file random berikut extension
            $filename = md5(time()) . '.' . $extension;
        
            // menyimpan cover ke folder public/img
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'logo';
            $uploaded_logo->move($destinationPath, $filename);
            
            // mengisi field cover di book dengan filename yang baru dibuat
            $partner->logo = $filename;
            $partner->save();
        }
        
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan logo $partner->nama_perusahaan"
        ]);
        
        return redirect()->route('partners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = partner::find($id);
        return view('partners.edit')->with(compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
       {
          $this->validate($request, [
                      'nama_perusahaan' => 'required|unique:partners,nama_perusahaan,' . $id,
                      'logo' => 'image|max:2048'
                  ]);
                  
                  $partner = partner::find($id);
                  $partner->update($request->all());
                  
                  if ($request->hasFile('logo')) {
                  // menambil cover yang diupload berikut ekstensinya
                      $filename = null;
                      $uploaded_logo = $request->file('logo');
                      $extension = $uploaded_logo->getClientOriginalExtension();
                  
                  // membuat nama file random dengan extension
                      $filename = md5(time()) . '.' . $extension;
                      $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'logo';
                  
                  // memindahkan file ke folder public/img
                      $uploaded_logo->move($destinationPath, $filename);
                  
                  // hapus cover lama, jika ada
                      if ($partner->logo) {
                      $old_logo = $partner->logo;
                      $filepath = public_path() . DIRECTORY_SEPARATOR . 'logo'
                          . DIRECTORY_SEPARATOR . $partner->logo;
                  
                      try {
                          File::delete($filepath);
                      } catch (FileNotFoundException $e) {
                  // File sudah dihapus/tidak ada
                      }
                  }
                  // ganti field cover dengan cover yang baru
                      $partner->logo = $filename;
                      $partner->save();
                  }
                  
                  Session::flash("flash_notification", [
                      "level"=>"success",
                      "message"=>"Berhasil menyimpan logo $partner->nama_perusahaan"
                  ]);
                  
                  return redirect()->route('partners.index');
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = partner::find($id);
    
        // hapus cover lama, jika ada
        if ($partner->logo) {
            $old_logo = $partner->logo;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'logo'
                . DIRECTORY_SEPARATOR . $partner->logo;
    
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
    // File sudah dihapus/tidak ada
        }
    }
    $partner->delete();
    
    Session::flash("flash_notification", [
        "level"=>"success",
        "message"=>"Logo berhasil dihapus"
    ]);
    
    return redirect()->route('partners.index');
    }
}
