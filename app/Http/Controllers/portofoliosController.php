<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use App\portofolio;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class portofoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
        {
            if ($request->ajax()) {
                $portofolios = portofolio::select(['id', 'keterangan', 'gambar']);
                return Datatables::of($portofolios)
                    ->addColumn('action', function($portofolio){
                        return view('datatable._action', [
                            'model' => $portofolio,
                            'form_url' => route('portofolios.destroy', $portofolio->id),
                            'edit_url' => route('portofolios.edit', $portofolio->id),
                            'confirm_message' => 'Yakin mau menghapus ' . $portofolio->keterangan . '?'
                        ]);
                    })->make(true);
            }

    $html = $htmlBuilder
    ->addColumn(['data' => 'keterangan', 'name'=>'keterangan', 'title'=>'Keterangan'])
    ->addColumn(['data' => 'gambar', 'name'=>'gambar', 'title'=>'Gambar'])
    ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);    

        return view('portofolios.index')->with(compact('html'));
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portofolios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
        {
           $this->validate($request, [
           'keterangan' => 'required|unique:portofolios,keterangan',
           'gambar' => 'image|max:2048'
           ]);

           $portofolio = portofolio::create($request->except('gambar'));
           
           // isi field cover jika ada cover yang diupload
           if ($request->hasFile('gambar')) {
           
           // Mengambil file yang diupload
           $uploaded_gambar = $request->file('gambar');
           
           // mengambil extension file
           $extension = $uploaded_gambar->getClientOriginalExtension();
           
           // membuat nama file random berikut extension
           $filename = md5(time()) . '.' . $extension;
           
           // menyimpan cover ke folder public/img
           $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'portofolio';
           $uploaded_gambar->move($destinationPath, $filename);
           
           // mengisi field cover di book dengan filename yang baru dibuat
           $portofolio->gambar = $filename;
           $portofolio->save();
           }
           
           Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menyimpan gambar $portofolio->keterangan"
           ]);
           
           return redirect()->route('portofolios.index');
           }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $portofolio = portofolio::find($id);
              return view('portofolios.edit')->with(compact('portofolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
        {
           $this->validate($request, [
                       'keterangan' => 'required|unique:portofolios,keterangan,' . $id,
                       'gambar' => 'image|max:2048'
                   ]);
                   
                   $portofolio = portofolio::find($id);
                   $portofolio->update($request->all());
                   
                   if ($request->hasFile('gambar')) {
                   // menambil cover yang diupload berikut ekstensinya
                       $filename = null;
                       $uploaded_gambar = $request->file('gambar');
                       $extension = $uploaded_gambar->getClientOriginalExtension();
                   
                   // membuat nama file random dengan extension
                       $filename = md5(time()) . '.' . $extension;
                       $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'portofolio';
                   
                   // memindahkan file ke folder public/img
                       $uploaded_gambar->move($destinationPath, $filename);
                   
                   // hapus cover lama, jika ada
                       if ($portofolio->gambar) {
                       $old_gambar = $portofolio->gambar;
                       $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                           . DIRECTORY_SEPARATOR . $portofolio->gambar;
                   
                       try {
                           File::delete($filepath);
                       } catch (FileNotFoundException $e) {
                   // File sudah dihapus/tidak ada
                       }
                   }
                   // ganti field cover dengan cover yang baru
                       $portofolio->gambar = $filename;
                       $portofolio->save();
                   }
                   
                   Session::flash("flash_notification", [
                       "level"=>"success",
                       "message"=>"Berhasil menyimpan $portofolio->keterangan"
                   ]);
                   
                   return redirect()->route('portofolios.index');
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
        {
            $portofolio = portofolio::find($id);
            
            // hapus cover lama, jika ada
            if ($portofolio->gambar) {
            $old_gambar = $portofolio->gambar;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'portofolio'
            . DIRECTORY_SEPARATOR . $portofolio->gambar;
            
            try {
            File::delete($filepath);
            } catch (FileNotFoundException $e) {
            // File sudah dihapus/tidak ada
            }
            }
            
            $portofolio->delete();
            
            Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Gambar berhasil dihapus"
            ]);
            return redirect()->route('portofolios.index');
        }
}
