<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\testi;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class testisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request, Builder $htmlBuilder)
           {
               if ($request->ajax()) {
               $testis = testi::select(['id', 'nama', 'jabatan', 'testi', 'foto']);
               return Datatables::of($testis)
                ->addColumn('action', function($testi){
                    return view('datatable._action', [
                        'model' => $testi,
                        'form_url' => route('testimoni.destroy', $testi->id),
                        'edit_url' => route('testimoni.edit', $testi->id),
                        'confirm_message' => 'Yakin mau menghapus testi ' . $testi->nama . '?'
                        ]);
                    })->make(true);
               }

           
               $html = $htmlBuilder
                   ->addColumn(['data' => 'nama', 'name'=>'nama', 'title'=>'Nama'])
                   ->addColumn(['data' => 'jabatan', 'name'=>'jabatan', 'title'=>'Jabatan'])
                   ->addColumn(['data' => 'testi', 'name'=>'testi', 'title'=>'Testi'])
                   ->addColumn(['data' => 'foto', 'name'=>'foto', 'title'=>'Foto'])
                   ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'', 'orderable'=>false, 'searchable'=>false]);
               
               return view('testis.index')->with(compact('html'));
           }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('testis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $input = $request->all();
               
              if ($request['foto']) {
                  if ($request->hasFile('foto')) {
                    
                      $getimageName = time().'.'.$request['foto']->getClientOriginalExtension();
                      $image = $request['foto']->move('testi/', $getimageName);
                      $request['foto'] = $getimageName;
                      $input['foto'] = $request['foto'];
                  }
              }
              else{
                  unset($request['foto']);
              }

              testi::create($input);
               
               Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Berhasil menyimpan foto $request->keterangan"
               ]);
               
               return redirect()->route('testimoni.index');
               
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testi = testi::find($id);
        return view('testimoni.edit')->with(compact('testi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['nama' => 'required|unique:testes,nama,'. $id]);
        $this->validate($request, ['jabatan' => 'required|unique:testes,jabatan,'. $id]);
        $this->validate($request, ['testi' => 'required|unique:testes,testi,'. $id]);
        
        $input = $request->all();
        $testi = testi::find($id);

        if ($request->foto) {
            if ($request->hasFile('foto')) {
                $path = 'testi/'.$testi->foto;
                if(file_exists($path)) {
                   unlink($path);
                }
                $getimageName = time().'.'.$request->foto->getClientOriginalExtension();
                $image = $request->foto->move('testi/', $getimageName);
                $request->foto = $getimageName;
                $input['foto'] = $request->foto;
            }
        }
        else{
            unset($request->foto);
        }

        $testi->update($input);
        
        Session::flash("flash_notification", [
           "level"=>"success",
           "message"=>"Berhasil mengedit testi $testi->nama"
       ]);
       return redirect()->route('testimoni.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        testi::destroy($id);
        Session::flash("flash_notification", [
        "level"=>"success",
        "message"=>"Testi berhasil dihapus"
        ]);
        return redirect()->route('testimoni.index');
    }
}
