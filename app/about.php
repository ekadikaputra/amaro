<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class about extends Model
{
    protected $fillable = ['visi', 'misi', 'nilai_perusahaan', 'profil'];
    
}
