<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
     protected $fillable = ['fullname', 'email', 'message', 'created_at'];
     public $timestamps = false;

}
