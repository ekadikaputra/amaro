<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galeri extends Model
{
     protected $fillable = ['lokasi', 'keterangan', 'gambar', 'pemberi_tugas','poisis_kontraktor','waktu_pelaksanaan','nomor_kontrak','nilai_kontrak'];
}
