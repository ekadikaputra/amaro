<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kontak extends Model
{
    protected $fillable = ['no_hp', 'jam_kerja', 'alamat'];
}
