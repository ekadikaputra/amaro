<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testi extends Model
{
    protected $table = 'testes';
    protected $fillable = ['nama', 'jabatan', 'testi', 'foto'];
    
}
