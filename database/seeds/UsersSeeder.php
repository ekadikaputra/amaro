<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Admin";
		$adminRole->save();
		
		// Membuat role member
		$memberRole = new Role();
		$memberRole->name = "member";
		$memberRole->display_name = "Member";
		$memberRole->save();
		
		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin Amaro';
		$admin->email = 'admin@amaro.com';
		$admin->password = bcrypt('qwerty123');
		$admin->save();
		$admin->attachRole($adminRole);
		
		// Membuat sample member
		$member = new User();
		$member->name = "Sample Member";
		$member->email = 'member@amaro.com';
		$member->password = bcrypt('dikaaa');
		$member->save();
		$member->attachRole($memberRole);
    }
}
