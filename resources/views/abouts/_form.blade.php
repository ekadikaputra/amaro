<div class="form-group{{ $errors->has('visi') ? ' has-error' : '' }}">
	{!! Form::label('visi', 'Visi', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('visi', null, ['class'=>'form-control']) !!}
		{!! $errors->first('visi', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('misi') ? ' has-error' : '' }}">
	{!! Form::label('misi', 'Misi', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('misi', null, ['class'=>'form-control']) !!}
		{!! $errors->first('misi', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('nilai_perusahaan') ? ' has-error' : '' }}">
	{!! Form::label('nilai_perusahaan', 'Nilai Perusahaan', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('nilai_perusahaan', null, ['class'=>'form-control']) !!}
		{!! $errors->first('nilai_perusahaan', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('profil') ? ' has-error' : '' }}">
	{!! Form::label('profil', 'Profil', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('profil', null, ['class'=>'form-control']) !!}
		{!! $errors->first('profil', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-4 col-md-offset-2">
		{!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
	</div>
</div>


		
		


		
		