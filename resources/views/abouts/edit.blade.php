@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/abouts') }}">About</a></li>
					<li class="active">Ubah About</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Ubah About</h2>
					</div>

					<div class="panel-body">
						{!! Form::model($about, ['url' => route('abouts.update', $about->id),
							'method'=>'put', 'class'=>'form-horizontal']) !!}
						@include('abouts._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection