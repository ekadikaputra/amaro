{!! Form::model($model, ['url' => $form_url, 'method' => 'delete', 'class' => 'form-inline js-confirm', 'data-confirm' => $confirm_message] ) !!}
	<a class="btn btn-info btn-sm" href="{{ $edit_url }}">Ubah</a>
{!! Form::close()!!}