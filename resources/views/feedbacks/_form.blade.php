<div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
	{!! Form::label('fullname', 'Nama Lengkap', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('fullname', null, ['class'=>'form-control']) !!}
		{!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	{!! Form::label('email', 'Email', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('email', null, ['class'=>'form-control']) !!}
		{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
	{!! Form::label('message', 'Message', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::text('message', null, ['class'=>'form-control']) !!}
		{!! $errors->first('message', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group">
	<div class="col-md-4 col-md-offset-2">
		{!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
	</div>
</div>


		
		


		
		