@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/feedbacks') }}">Feedback</a></li>
					<li class="active">Ubah Feedback</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Ubah Feedback</h2>
					</div>

					<div class="panel-body">
						{!! Form::model($Feedback, ['url' => route('kontaks.update', $Feedback->id),
							'method'=>'put', 'class'=>'form-horizontal']) !!}
						@include('kontaks._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection