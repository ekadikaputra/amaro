<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
	{!! Form::label('keterangan', 'Keterangan', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('keterangan', null, ['class'=>'form-control']) !!}
	{!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('lokasi') ? ' has-error' : '' }}">
	{!! Form::label('lokasi', 'Lokasi', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('lokasi', null, ['class'=>'form-control']) !!}
	{!! $errors->first('lokasi', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('pemberi_tugas') ? ' has-error' : '' }}">
	{!! Form::label('pemberi_tugas', 'Pemberi Tugas', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('pemberi_tugas', null, ['class'=>'form-control']) !!}
	{!! $errors->first('pemberi_tugas', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('posisi_kontraktor') ? ' has-error' : '' }}">
	{!! Form::label('posisi_kontraktor', 'Posisi Kontraktor', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('posisi_kontraktor', null, ['class'=>'form-control']) !!}
	{!! $errors->first('posisi_kontraktor', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('nomor_kontrak') ? ' has-error' : '' }}">
	{!! Form::label('nomor_kontrak', 'Nomor Kontrak', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('nomor_kontrak', null, ['class'=>'form-control']) !!}
	{!! $errors->first('nomor_kontrak', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('nilai_kontrak') ? ' has-error' : '' }}">
	{!! Form::label('nilai_kontrak', 'Nilai Kontrak', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('nilai_kontrak', null, ['class'=>'form-control']) !!}
	{!! $errors->first('nilai_kontrak', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('waktu_pelaksanaan') ? ' has-error' : '' }}">
	{!! Form::label('waktu_pelaksanaan', 'Waktu Pelaksanaan', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('waktu_pelaksanaan', null, ['class'=>'form-control']) !!}
	{!! $errors->first('waktu_pelaksanaan', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
	{!! Form::label('gambar', 'Gambar', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::file('gambar') !!}
		@if (isset($galeri) && $galeri->gambar)
		<p>
		{!! Html::image(asset('galeri/'.$galeri->gambar), null, ['class'=>'img-rounded img-responsive']) !!}
		</p>
		@endif
		{!! $errors->first('gambar', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group">
<div class="col-md-4 col-md-offset-2">
{!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
</div>