
<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
      <!-- meta character set -->
        <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>AMARO</title>    
    <!-- Meta Description -->
        <meta name="description" content="AMARO">
        {{-- <meta name="keywords" content="">
        <meta name="author" content=""> --}}
    
    <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSS
    ================================================== -->
    
    <link href='{{ url('http://fonts.googleapis.com/css?family=Open+Sans:400,300,700')}}' rel='stylesheet' type='text/css'>
    
    <!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css')}}">
    <!-- bootstrap.min -->
        <link rel="stylesheet" href="{{ asset('front/css/jquery.fancybox.css')}}">
    <!-- bootstrap.min -->
        <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css')}}">
    <!-- bootstrap.min -->
        <link rel="stylesheet" href="{{ asset('front/css/owl.carousel.css')}}">
    <!-- bootstrap.min -->
        <link rel="stylesheet" href="{{ asset('front/css/slit-slider.css')}}">
    <!-- bootstrap.min -->
        <link rel="stylesheet" href="{{ asset('front/css/animate.css')}}">
    <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{ asset('front/css/main.css')}}">

    <!-- Modernizer Script for old Browsers -->
        <script src="{{ asset('front/js/modernizr-2.6.2.min.js')}}"></script>

    </head>
  
    <body id="body">

   {{--  <!-- preloader -->
    <div id="preloader">
            <div class="loder-box">
              <div class="battery"></div>
            </div>
    </div>
    <!-- end preloader --> --}}

        <!--
        Fixed Navigation
        ==================================== -->
        <header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
                    </button>
          <!-- /responsive nav button -->
          
          <!-- logo -->
          <h1 class="navbar-brand">
            <a href="#body">AMARO</a>
          </h1>
          <!-- /logo -->
                </div>

        <!-- main nav -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <ul id="nav" class="nav navbar-nav">
                        <li><a href="#body">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#gallery">Gallery</a></li>
                        <li><a href="#portfolio">portfolio</a></li>
                        <li><a href="#testimonials">Testimonial</a></li>
                        {{-- <li><a href="#price">price</a></li> --}}
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
        <!-- /main nav -->
        
            </div>
        </header>
        <!--
        End Fixed Navigation
        ==================================== -->
    
    <main class="site-content" role="main">
    
        <!--
        Home Slider
        ==================================== -->
    
    <section id="home-slider">
            <div id="slider" class="sl-slider-wrapper">

        <div class="sl-slider">
        
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

            <div class="bg-img bg-img-1"></div>

            <div class="slide-caption">
                <div class="caption-content">
                    <h2 class="animated fadeInDown">AMARO</h2>
                    <span class="animated fadeInDown">Professional Consultant</span>
                    <a href="#" class="btn btn-blue btn-effect">Join US</a>
                </div>
            </div>
            
          </div>
          
          <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
          
            <div class="bg-img bg-img-2"></div>
            <div class="slide-caption">
                <div class="caption-content">
                    <h2>AMARO</h2>
                    <span>Professional Consultant</span>
                    <a href="#" class="btn btn-blue btn-effect">Join US</a>
                </div>
            </div>
            
          </div>
          
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
            
            <div class="bg-img bg-img-3"></div>
            <div class="slide-caption">
                <div class="caption-content">
                    <h2>AMARO</h2>
                    <span>Professional Consultant</span>
                    <a href="#" class="btn btn-blue btn-effect">Join US</a>
                </div>
            </div>

          </div>

        </div><!-- /sl-slider -->

                <!-- 
                <nav id="nav-arrows" class="nav-arrows">
                    <span class="nav-arrow-prev">Previous</span>
                    <span class="nav-arrow-next">Next</span>
                </nav>
                -->
                
                <nav id="nav-arrows" class="nav-arrows hidden-xs hidden-sm visible-md visible-lg">
                    <a href="javascript:;" class="sl-prev">
                        <i class="fa fa-angle-left fa-3x"></i>
                    </a>
                    <a href="javascript:;" class="sl-next">
                        <i class="fa fa-angle-right fa-3x"></i>
                    </a>
                </nav>
                

        <nav id="nav-dots" class="nav-dots visible-xs visible-sm hidden-md hidden-lg">
          <span class="nav-dot-current"></span>
          <span></span>
          <span></span>
        </nav>

      </div><!-- /slider-wrapper -->
    </section>
    
        <!--
        End Home SliderEnd
        ==================================== -->
      
      <!-- About section -->
      <section id="about">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>ABOUT</h2>
            </div>
            
            <div class="col-md-6 wow animated fadeInUp">
              <div class="price-table text-center">
                <span>Vision</span>
                <div class="value">
                  <i class="fa fa-binoculars fa-3x" aria-hidden="true"></i>                  
                </div>
                <p style="margin: 10px;text-align: justify;">{!!$about->visi!!}</p>
              </div>
            </div>
            
            <div class="col-md-6 wow animated fadeInUp" data-wow-delay="0.6s">
              <div class="price-table text-center">
                <span>Mision</span>
                <div class="value">
                  <i class="fa fa-plane fa-3x"></i>                  
                </div>
                <p style="margin: 10px;text-align: justify;">{!!$about->misi!!}</p>
              </div>
            </div>
      
          </div>
        </div>
      </section>
      <!-- end About section -->

      <!-- Gallery section -->
      <section id="gallery"  style="background-color: #009EE3">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>GALLERY</h2>
            </div>           

            <ul class="project-wrapper wow animated fadeInUp">
              @foreach ($porto as $por)
              <li class="portfolio-item">
                <img src="{{asset('portofolio/'.$por->gambar)}}" class="img-fullwidth" alt="" height="300">
                <figcaption class="mask">
                  <h3>{{$por->keterangan}}</h3>
                  
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Araund The world" data-fancybox-group="works" href="{{asset('portofolio/'.$por->gambar)}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              @endforeach
              
            </ul>
            
          </div>
        </div>
      </section>
      <!-- end Gallery section -->

      <!-- portfolio section -->
      <section id="portfolio">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>PORTFOLIO</h2>
            </div>
            <ul class="project-wrapper wow animated fadeInUp">
              @foreach ($gallery as $key=>$gal)
              <li class="portfolio-item">
                <img src="{{ asset('galeri/'.$gal->gambar)}}" class="img-fullwidth" alt="" height="300">
                <figcaption class="mask">
                  <h3>{!!$gal->keterangan!!}</h3>
                  <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-{{$key}}">More</button>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="{{$gal->keterangan}}" data-fancybox-group="works" href="{{ asset('galeri/'.$gal->gambar)}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              @endforeach             
              
            </ul>
            
          </div>
        </div>
      </section>
      @foreach ($gallery as $key=>$gal)
        <!-- Modal -->
        <div id="modal-{{$key}}" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{$gal->keterangan}}</h4>
              </div>
              <div class="modal-body">
                <table width="100%">
                  <tr>
                    <td width="30%">Pemberi Tugas</td>
                    <td width="2%">:</td>
                    <td width="68%">{{$gal->pemberi_tugas}}</td>
                  </tr>
                  <tr>
                    <td width="30%">Posisi Kontraktor</td>
                    <td width="2%">:</td>
                    <td width="68%">{{$gal->posisi_kontraktor}}</td>
                  </tr>
                  <tr>
                    <td width="30%">Waktu Pelaksanaan</td>
                    <td width="2%">:</td>
                    <td width="68%">{{$gal->waktu_pelaksanaan}}</td>
                  </tr>
                  <tr>
                    <td width="30%">Nomor Kontrak</td>
                    <td width="2%">:</td>
                    <td width="68%">{{$gal->nomor_kontrak}}</td>
                  </tr>
                  <tr>
                    <td width="30%">Nilai Kontrak</td>
                    <td width="2%">:</td>
                    <td width="68%">{{$gal->nilai_kontrak}}</td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
      @endforeach      
      <!-- end portfolio section -->

      <!-- Testimonial section -->
      <section id="testimonials" class="parallax">
        <div class="overlay">
          <div class="container">
            <div class="row">
            
              <div class="sec-title text-center white wow animated fadeInDown">
                <h2>TESTIMONIAL</h2>
              </div>
              
              <div id="testimonial" class=" wow animated fadeInUp">
                @foreach ($testi as $test)
                <div class="testimonial-item text-center">
                  <img src="{{asset('front/img/member-1.jpg')}}" alt="Our Clients">
                  <div class="clearfix">
                    <span>{{$test->nama}}</span>
                    <p>{!!$test->testi!!}</p>
                  </div>
                </div>
                @endforeach
              </div>
            
            </div>
          </div>
        </div>
      </section>
      <!-- end Testimonial section -->

{{--       <!-- about section -->
      <section id="about" >
        <div class="container">
          <div class="row">
            <div class="col-md-4 wow animated fadeInLeft">
              <div class="recent-works">
                <h3>Recent Works</h3>
                <div id="works">
                  <div class="work-item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br> <br> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
                  <div class="work-item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br><br> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
                  <div class="work-item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br><br> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7 col-md-offset-1 wow animated fadeInRight">
              <div class="welcome-block">
                <h3>Welcome To Our Site</h3>                
                   <div class="message-body">
                  <img src="{{asset('front/img/member-1.jpg')}}" class="pull-left" alt="member">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                   </div>
                    <a href="#" class="btn btn-border btn-effect pull-right">Read More</a>
                </div>
            </div>
          </div>
        </div>
      </section>
      <!-- end about section -->
      
      
      <!-- Service section -->
      <section id="service">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center">
              <h2 class="wow animated bounceInLeft">Service</h2>
              <p class="wow animated bounceInRight">The Key Features of our Job</p>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn">
              <div class="service-item">
                <div class="service-icon">
                  <i class="fa fa-home fa-3x"></i>
                </div>
                <h3>Support</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
              </div>
            </div>
          
            <div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.3s">
              <div class="service-item">
                <div class="service-icon">
                  <i class="fa fa-tasks fa-3x"></i>
                </div>
                <h3>Well Documented</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
              </div>
            </div>
          
            <div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.6s">
              <div class="service-item">
                <div class="service-icon">
                  <i class="fa fa-clock-o fa-3x"></i>
                </div>
                <h3>Design UI/UX</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
              </div>
            </div>
          
            <div class="col-md-3 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.9s">
              <div class="service-item">
                <div class="service-icon">
                  <i class="fa fa-heart fa-3x"></i>
                </div>
                
                <h3>Web Security</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>              
              </div>
            </div>
            
          </div>
        </div>
      </section>
      <!-- end Service section -->
      
      <!-- portfolio section -->
      <section id="portfolio">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>FEATURED PROJECTS</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
            

            <ul class="project-wrapper wow animated fadeInUp">
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item.jpg')}}" class="img-responsive" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Araund The world" data-fancybox-group="works" href="{{asset('front/img/portfolio/item.jpg')}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item2.jpg')}}" class="img-responsive" alt="Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. ">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Wall street" href="{{asset('front/img/slider/banner.jpg')}}" data-fancybox-group="works" ><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item3.jpg')}}" class="img-responsive" alt="Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. ">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Behind The world" data-fancybox-group="works" href="{{asset('front/img/portfolio/item3.jpg')}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item4.jpg')}}" class="img-responsive" alt="Lorem Ipsum is simply dummy text of the printing and typesetting ndustry.">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Wall street 4" data-fancybox-group="works" href="{{asset('front/img/portfolio/item4.jpg')}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item5.jpg')}}" class="img-responsive" alt="Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. ">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Wall street 5" data-fancybox-group="works" href="{{asset('front/img/portfolio/item5.jpg')}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
              
              <li class="portfolio-item">
                <img src="{{asset('front/img/portfolio/item6.jpg')}}" class="img-responsive" alt="Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. ">
                <figcaption class="mask">
                  <h3>Wall street</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting ndustry. </p>
                </figcaption>
                <ul class="external">
                  <li><a class="fancybox" title="Wall street 6" data-fancybox-group="works" href="{{asset('front/img/portfolio/item6.jpg')}}"><i class="fa fa-search"></i></a></li>
                  <li><a href=""><i class="fa fa-link"></i></a></li>
                </ul>
              </li>
            </ul>
            
          </div>
        </div>
      </section>
      <!-- end portfolio section -->
      
      <!-- Testimonial section -->
      <section id="testimonials" class="parallax">
        <div class="overlay">
          <div class="container">
            <div class="row">
            
              <div class="sec-title text-center white wow animated fadeInDown">
                <h2>What people say</h2>
              </div>
              
              <div id="testimonial" class=" wow animated fadeInUp">
                <div class="testimonial-item text-center">
                  <img src="{{asset('front/img/member-1.jpg')}}" alt="Our Clients">
                  <div class="clearfix">
                    <span>Katty Flower</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                  </div>
                </div>
                <div class="testimonial-item text-center">
                  <img src="{{asset('front/img/member-1.jpg')}}" alt="Our Clients">
                  <div class="clearfix">
                    <span>Katty Flower</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                  </div>
                </div>
                <div class="testimonial-item text-center">
                  <img src="{{asset('front/img/member-1.jpg')}}" alt="Our Clients">
                  <div class="clearfix">
                    <span>Katty Flower</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
      </section>
      <!-- end Testimonial section -->
      
      <!-- Price section -->
      <section id="price">
        <div class="container">
          <div class="row">
          
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>Price</h2>
              <p>Our price for your company</p>
            </div>
            
            <div class="col-md-4 wow animated fadeInUp">
              <div class="price-table text-center">
                <span>Silver</span>
                <div class="value">
                  <span>$</span>
                  <span>24,35</span><br>
                  <span>month</span>
                </div>
                <ul>
                  <li>No Bonus Points</li>
                  <li>No Bonus Points</li>
                  <li>No Bonus Points</li>
                  <li>No Bonus Points</li>
                  <li><a href="#">sign up</a></li>
                </ul>
              </div>
            </div>
            
            <div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.4s">
              <div class="price-table featured text-center">
                <span>Gold</span>
                <div class="value">
                  <span>$</span>
                  <span>50,00</span><br>
                  <span>month</span>
                </div>
                <ul>
                  <li>Free Trial</li>
                  <li>Free Trial</li>
                  <li>Free Trial</li>
                  <li>Free Trial</li>
                  <li><a href="#">sign up</a></li>
                </ul>
              </div>
            </div>
            
            <div class="col-md-4 wow animated fadeInUp" data-wow-delay="0.8s">
              <div class="price-table text-center">
                <span>Diamond</span>
                <div class="value">
                  <span>$</span>
                  <span>123,12</span><br>
                  <span>month</span>
                </div>
                <ul>
                  <li>All Bonus Points</li>
                  <li>All Bonus Points</li>
                  <li>All Bonus Points</li>
                  <li>All Bonus Points</li>
                  <li><a href="#">sign up</a></li>
                </ul>
              </div>
            </div>
    
          </div>
        </div>
      </section>
      <!-- end Price section --> --}}
      
      {{-- <!-- Social section -->
      <section id="social" class="parallax">
        <div class="overlay">
          <div class="container">
            <div class="row">
            
              <div class="sec-title text-center white wow animated fadeInDown">
                <h2>FOLLOW US</h2>
                <p>Beautifully simple follow buttons to help you get followers on</p>
              </div>
              
              <ul class="social-button">
                <li class="wow animated zoomIn"><a href="#"><i class="fa fa-thumbs-up fa-2x"></i></a></li>
                <li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                <li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-dribbble fa-2x"></i></a></li>             
              </ul>
              
            </div>
          </div>
        </div>
      </section>
      <!-- end Social section --> --}}
      
      <!-- Contact section -->
      <section id="contact" >
        <div class="container">
          <div class="row">
            
            <div class="sec-title text-center wow animated fadeInDown">
              <h2>Contact</h2>
              <p>Leave a Message</p>
            </div>
            
            
            <div class="col-md-5 contact-form wow animated fadeInLeft">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3968.4828045283425!2d106.80081431425766!3d-6.166750462150889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f66ff202bf5f%3A0xf1139b899cdf2966!2sITC+Roxy+Mas!5e0!3m2!1sen!2sid!4v1508853209644" width="450" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              <address class="contact-details">
                <h3>Contact Us</h3>           
                <p><i class="fa fa-pencil"></i>{{$contact->alamat}}</p><br>
                <p><i class="fa fa-phone"></i>Phone: {{$contact->no_hp}} </p>
                {{-- <p><i class="fa fa-envelope"></i>website@yourname.com</p> --}}
              </address>
            </div>
            
            <div class="col-md-7 wow animated fadeInRight">
              @if(session()->has('pesan'))
                  <div class="alert alert-success">
                      {{ session()->get('pesan') }}
                  </div>
              @endif
              <form action="{{url('/')}}" method="post">
                {{csrf_field()}}
                <div class="input-field">
                  <input type="text" name="fullname" class="form-control" placeholder="Your Name..." required>
                </div>
                <div class="input-field">
                  <input type="email" name="email" class="form-control" placeholder="Your Email..." required>
                </div>
                <div class="input-field">
                  <textarea name="message" class="form-control" placeholder="Messages..." required></textarea>
                </div>
                    <button type="submit" id="submit" class="btn btn-blue btn-effect">Send</button>
              </form>
              
            </div>
      
          </div>
        </div>
      </section>
      <!-- end Contact section -->
      
    {{--   <section id="google-map">
        <div id="map-canvas" class="wow animated fadeInUp"></div>
      </section> --}}
    
    </main>
    
    <footer id="footer">
      <div class="container">
        <div class="row text-center">
          <div class="footer-content">
            <div class="footer-social">
              <ul>
                <li class="wow animated zoomIn"><a href="#"><i class="fa fa-facebook fa-3x"></i></a></li>
                <li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="#"><i class="fa fa-twitter fa-3x"></i></a></li>
                <li class="wow animated zoomIn" data-wow-delay="0.6s"><a href="#"><i class="fa fa-instagram fa-3x"></i></a></li>
              </ul>
            </div>
            
            <p>Copyright &copy; 2017 Amaro </p>
          </div>
        </div>
      </div>
    </footer>
    
    <!-- Essential jQuery Plugins
    ================================================== -->
    <!-- Main jQuery -->
        <script src="{{ asset('front/js/jquery-1.11.1.min.js')}}"></script>
    <!-- Twitter Bootstrap -->
        <script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
    <!-- Single Page Nav -->
        <script src="{{ asset('front/js/jquery.singlePageNav.min.js')}}"></script>
    <!-- jquery.fancybox.pack -->
        <script src="{{ asset('front/js/jquery.fancybox.pack.js')}}"></script>
    <!-- Google Map API -->
    {{-- <script src="{{ url('http://maps.google.com/maps/api/js?sensor=false')}}"></script> --}}
    <!-- Owl Carousel -->
        <script src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
        <!-- jquery easing -->
        <script src="{{ asset('front/js/jquery.easing.min.js')}}"></script>
        <!-- Fullscreen slider -->
        <script src="{{ asset('front/js/jquery.slitslider.js')}}"></script>
        <script src="{{ asset('front/js/jquery.ba-cond.min.js')}}"></script>
    <!-- onscroll animation -->
        <script src="{{ asset('front/js/wow.min.js')}}"></script>
    <!-- Custom Functions -->
        <script src="{{ asset('front/js/main.js')}}"></script>
    </body>
</html>