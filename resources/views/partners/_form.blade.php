<div class="form-group{{ $errors->has('nama_perusahaan') ? ' has-error' : '' }}">
	{!! Form::label('nama_perusahaan', 'Nama Perusahaan', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
	{!! Form::text('nama_perusahaan', null, ['class'=>'form-control']) !!}
	{!! $errors->first('nama_perusahaan', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
	{!! Form::label('logo', 'Logo', ['class'=>'col-md-2 control-label']) !!}
	<div class="col-md-4">
		{!! Form::file('logo') !!}
		@if (isset($partner) && $partner->logo)
		<p>
		{!! Html::image(asset('logo/'.$partner->logo), null, ['class'=>'img-rounded img-responsive']) !!}
		</p>
		@endif
		{!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group">
<div class="col-md-4 col-md-offset-2">
{!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
</div>