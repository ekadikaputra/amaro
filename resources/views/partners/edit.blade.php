@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/partner') }}">Partner</a></li>
					<li class="active">Ubah Partner</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Ubah Partner</h2>
					</div>
				
					<div class="panel-body">
						{!! Form::model($partner, ['url' => route('partners.update', $partner->id),
							'method' => 'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
						@include('partners._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection