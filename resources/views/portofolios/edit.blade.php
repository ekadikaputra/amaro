@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/portofolios') }}">Portofolio</a></li>
					<li class="active">Ubah Portofolio</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Ubah Portofolio</h2>
					</div>

					<div class="panel-body">
						{!! Form::model($portofolio, ['url' => route('portofolios.update', $portofolio->id),
							'method' => 'put', 'files'=>'true', 'class'=>'form-horizontal']) !!}
						@include('portofolios._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>	
	</div>
@endsection