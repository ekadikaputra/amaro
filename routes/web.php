<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('layout');
// });
Route::get('/', 'FrontController@index');
Route::post('/', 'FrontController@post');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix'=>'admin', 'middleware'=>['auth',]], function () {
	Route::resource('abouts', 'aboutsController');
	Route::resource('kontaks', 'kontaksController');
	Route::resource('galeris', 'galerisController');
	Route::resource('testimoni', 'testisController');
	Route::resource('partners', 'partnersController');
	Route::resource('feedbacks', 'feedbacksController');
	Route::resource('portofolios', 'portofoliosController');


});